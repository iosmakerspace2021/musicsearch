//
//  musicTableViewCell.swift
//  musicSearch123
//
//  Created by Olzhas Akhmetov on 31.03.2021.
//

import UIKit

class MusicTableViewCell: UITableViewCell {
    
    @IBOutlet weak var artworkIV: UIImageView!
    @IBOutlet weak var tacknameLabel: UILabel!
    @IBOutlet weak var artistnameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
