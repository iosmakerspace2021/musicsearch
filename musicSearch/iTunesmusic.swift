//
//  iTunesmusic.swift
//  musicSearch123
//
//  Created by Olzhas Akhmetov on 31.03.2021.
//

import UIKit
import SwiftyJSON

class iTunesmusic {
    var TrackName = ""
    var ArtistName = ""
    var Artwork = ""
    var PreviewUrl = ""
    
    init() {
        
    }
    
    init(json: JSON) {
        if let temp = json["artistName"].string {
            ArtistName = temp
        }
        if let temp = json["trackName"].string {
            TrackName = temp
        }
        if let temp = json["artworkUrl100"].string {
            Artwork = temp
        }
        if let temp = json["previewUrl"].string {
            PreviewUrl = temp
        }
    }

}
