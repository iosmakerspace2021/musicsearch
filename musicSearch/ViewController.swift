//
//  ViewController.swift
//  musicSearch123
//
//  Created by Olzhas Akhmetov on 31.03.2021.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var webview: WKWebView!
    
    var previewurl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let url = URL(string: previewurl)!
        
        let urlreq = URLRequest(url: url)
        
        webview.load(urlreq)
    }
}

