//
//  MusicTableViewController.swift
//  musicSearch123
//
//  Created by Olzhas Akhmetov on 31.03.2021.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import SDWebImage

class MusicTableViewController: UITableViewController, UISearchBarDelegate {
    
    var musicArray: [iTunesmusic] = []
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var searchArtist = "eminem"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        // 1
        //searchController.searchResultsUpdater = self
        searchController.searchBar.delegate  = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = "Search for artist"
        // 4
        navigationItem.searchController = searchController
        // 5
        definesPresentationContext = true
        
        downloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return musicArray.count
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print(searchBar.text)
        searchArtist = searchBar.text!
        musicArray.removeAll()
        tableView.reloadData()
        downloadData()
    }
    
    func downloadData() {
        let url = "https://itunes.apple.com/search?term=\(searchArtist.replacingOccurrences(of: " ", with: "+"))&limit=25"
        print(url)
        SVProgressHUD.show()

        AF.request(url, method: .get).responseJSON { response in
            
            print("\(String(describing: response.response))") // HTTP URL response
            print("\(String(describing: response.data))")     // server data
            print("\(response.result)")   // result of response serialization
            print("\(String(describing: response.value))")   // result of response serialization

            if response.response?.statusCode == 200 {
                print("response.value - \(response.value)")

                let json = JSON(response.value!)

                if let results = json["results"].array {
                    for track in results {
                        self.musicArray.append(iTunesmusic(json: track))
                    }
                }
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            } else  {
                SVProgressHUD.dismiss()
            }
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MusicTableViewCell

        cell.artistnameLabel.text = musicArray[indexPath.row].ArtistName
        
        cell.tacknameLabel.text = musicArray[indexPath.row].TrackName
        
        cell.artworkIV!.sd_setImage(with: URL(string: musicArray[indexPath.row].Artwork), completed: nil)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewcontroller = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        viewcontroller.previewurl = musicArray[indexPath.row].PreviewUrl
        
        navigationController?.pushViewController(viewcontroller, animated: true)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
